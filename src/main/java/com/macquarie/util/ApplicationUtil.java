package com.macquarie.util;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

import java.io.File;

public class ApplicationUtil {

    private File configFile = new File("perftest.conf");

    public ApplicationUtil() {
    }

    public String getTestEnvironment() {
        try {
            Config config = ConfigFactory.parseFile(configFile).getConfig("app");
            return config.getString("testEnvironment");
        }
        catch (Exception ignored) {
            return "";
        }
    }

    public String getRuntimeConfig(String key) {
        try {
            Config config = ConfigFactory.parseFile(configFile).getConfig("app");
            return config.getString("runtimeConfig." + key);
        }
        catch (Exception ignored) {
            return "";
        }
    }
}
