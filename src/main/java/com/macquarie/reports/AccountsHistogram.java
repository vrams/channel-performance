package com.macquarie.reports;

import com.macquarie.util.ApplicationUtil;
import org.HdrHistogram.Histogram;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

public class AccountsHistogram {

    private Map<String, Histogram> histograms = new HashMap<>();
    private ApplicationUtil appUtil = new ApplicationUtil();

    public void addHistograms(String testName, Histogram histogram) {
        this.histograms.put(testName, histogram);
    }

    public void generateHistogramFile() {
        try {
            String environment = appUtil.getTestEnvironment();
            String testType = appUtil.getRuntimeConfig("testType");
            File resultsFolder = FileUtils.getFile(appUtil.getRuntimeConfig("resultsFolder"),
                    appUtil.getRuntimeConfig("histogramFolder") + "_" +
                            LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss")));
            if (!resultsFolder.exists()) {
                FileUtils.forceMkdir(resultsFolder);
            }
            for (Map.Entry<String, Histogram> entry : histograms.entrySet()) {
                File histogramFile = FileUtils.getFile(resultsFolder, entry.getKey() + "_" + environment + "_" +
                        testType + ".hgrm");
                FileOutputStream outputStream = new FileOutputStream(histogramFile);
                entry.getValue().outputPercentileDistribution(new PrintStream(outputStream), 1000.0);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}
