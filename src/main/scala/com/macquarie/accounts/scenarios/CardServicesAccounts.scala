package com.macquarie.accounts.scenarios

import com.macquarie.accounts.config.Scenarios
import com.macquarie.accounts.handler.RequestHandler
import com.macquarie.accounts.util.AppConfig
import io.gatling.core.Predef._
import io.gatling.core.structure.ScenarioBuilder

object CardServicesAccounts {
    private val url: String = "/api/cards/rest/v2/accounts"
    private val accountFeed = csv(AppConfig.getTestVolumeConfig(Scenarios.CARD_SERVICES_ACCOUNTS.toString.toLowerCase(), "dataFile")).circular.random
    private val requestHandler: RequestHandler = new RequestHandler(url)

    def execute(): ScenarioBuilder = {
        val parameters = Map("mac" -> "${mac}")
        val positionsScenario: ScenarioBuilder = scenario("CardServices")
          .feed(accountFeed)
          .exec(requestHandler.getCardServicesAccountsRequest("CardServices", parameters))
        positionsScenario
    }
}
