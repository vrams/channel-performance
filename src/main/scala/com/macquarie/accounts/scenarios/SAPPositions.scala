package com.macquarie.accounts.scenarios

import com.macquarie.accounts.config.Scenarios
import com.macquarie.accounts.handler.RequestHandler
import com.macquarie.accounts.util.AppConfig
import io.gatling.core.Predef._
import io.gatling.core.structure.ScenarioBuilder

object SAPPositions {

    private val url: String = "/ACCOUNT_POSITION_API_SRV/accountpositionapi"
    private val userName: String = "GW_ENDUSER"
    private val password: String = "30oCTOBER@2017"
    private val accountFeed = csv(AppConfig.getTestVolumeConfig(Scenarios.SAP_POSITIONS.toString.toLowerCase(), "dataFile")).circular.random
    private val requestHandler: RequestHandler = new RequestHandler(url)

    def execute(): ScenarioBuilder = {
        val parameters = Map("$filter" -> "${accountId}")
        val positionsScenario: ScenarioBuilder = scenario("SAPPositions")
          .feed(accountFeed)
          .exec(requestHandler.getSAPRequest("SAPPositions", userName, password, parameters))
        positionsScenario
    }
}
