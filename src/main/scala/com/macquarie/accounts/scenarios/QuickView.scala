package com.macquarie.accounts.scenarios

import com.macquarie.accounts.config.Scenarios
import com.macquarie.accounts.handler.RequestHandler
import com.macquarie.accounts.util.AppConfig
import io.gatling.core.Predef._
import io.gatling.core.structure._

object QuickView {

    private val name: String = Scenarios.QUICK_VIEW.toString.toLowerCase()
    private val clientId: String = AppConfig.getTestVolumeConfig(name, "clientId")
    private val quickViewFeed = csv(AppConfig.getTestVolumeConfig(name, "dataFile")).circular.random
    private val positionsFeed = csv(AppConfig.getTestVolumeConfig(name, "dataFile")).circular.random
    private var requestHandler: RequestHandler = new RequestHandler("")

    def execute(): ScenarioBuilder = {
        val quickViewScenario: ScenarioBuilder = scenario("QuickView")
          .feed(quickViewFeed)
          .exec(quickView())
          .feed(positionsFeed)
          .exec(positions())
        quickViewScenario
    }

    private def quickView(): ChainBuilder = {
        val url: String = "/mobile/int/api/channel/v1/oauth20/token"
        requestHandler = new RequestHandler(url)
        val quickView: ChainBuilder = exec(registerUserRequest)
          .exec(setPinRequest)
          .exec(quickViewRequest)
        quickView
    }

    private def positions(): ChainBuilder = {
        val url: String = "/api/account-management/v1/positions"
        val parameters = Map("resource_id" -> "${accountId}")
        val headers = Map("Authorization" -> "Bearer ${accessToken}")
        requestHandler = new RequestHandler(url)
        val positions: ChainBuilder = requestHandler.getRequestWithHeaders("Positions", parameters, headers)
        positions
    }

    private val registerUserRequest: ChainBuilder = exec(
        requestHandler.postRequest("Register", Map(
            "client_id" -> clientId,
            "grant_type" -> "password",
            "username" -> "${macId}",
            "password" -> "${password}",
            "pin_enabled" -> "false",
            "device_name" -> "MyPhoneType",
            "device_type" -> "MyPhoneVersion",
            "operation" -> "register"
        ))
    )

    private val setPinRequest: ChainBuilder = exec(
        requestHandler.postRequest("SetPIN", Map(
            "client_id" -> clientId,
            "grant_type" -> "refresh_token",
            "pin_enabled" -> "false",
            "operation" -> "register",
            "refresh_token" -> "${refreshToken}",
            "pin" -> "1234"
        ))
    )

    private val quickViewRequest: ChainBuilder = exec(
        requestHandler.postRequest("QuickView", Map(
            "client_id" -> clientId,
            "grant_type" -> "refresh_token",
            "pin_enabled" -> "true",
            "device_name" -> "MyPhoneType",
            "device_type" -> "MyPhoneVersion",
            "operation" -> "validate",
            "refresh_token" -> "${refreshToken}",
            "username" -> "${macId}",
            "password" -> "${password}"
        ))
    )
}
