package com.macquarie.accounts.scenarios

import com.macquarie.accounts.config.Scenarios
import com.macquarie.accounts.handler.RequestHandler
import com.macquarie.accounts.util.AppConfig
import io.gatling.core.Predef._
import io.gatling.core.structure.ScenarioBuilder

object SSLSAPPositions {
    private val url: String = "/ACCOUNT_POSITION_API_SRV/accountpositionapi"
    private val accountFeed = csv(AppConfig.getTestVolumeConfig(Scenarios.SAP_POSITIONS.toString.toLowerCase(), "dataFile")).circular.random
    private val requestHandler: RequestHandler = new RequestHandler(url)

    def execute(): ScenarioBuilder = {
        val parameters = Map("$filter" -> "${accountId}")
        val positionsScenario: ScenarioBuilder = scenario("SAPPositions")
          .feed(accountFeed)
          .exec(requestHandler.getSSLSAPRequest("SAPPositions", parameters))
        positionsScenario
    }
}
