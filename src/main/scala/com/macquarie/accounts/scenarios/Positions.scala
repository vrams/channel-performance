package com.macquarie.accounts.scenarios

import com.macquarie.accounts.config.Scenarios
import com.macquarie.accounts.handler.RequestHandler
import com.macquarie.accounts.util.AppConfig
import io.gatling.core.Predef._
import io.gatling.core.structure.ScenarioBuilder

object Positions {

    private val url: String = "/account-management/v1/positions"
    private val accountFeed = csv(AppConfig.getTestVolumeConfig(Scenarios.POSITIONS.toString.toLowerCase(), "dataFile")).circular.random
    private val requestHandler: RequestHandler = new RequestHandler(url)

    def execute(): ScenarioBuilder = {
        val parameters = Map("resource_id" -> "${accountId}")
        val positionsScenario: ScenarioBuilder = scenario("Positions")
          .feed(accountFeed)
          .exec(requestHandler.getRequest("Positions", parameters))
        positionsScenario
    }
}
