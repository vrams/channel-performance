package com.macquarie.accounts.scenarios

import com.macquarie.accounts.config.Scenarios
import com.macquarie.accounts.handler.RequestHandler
import com.macquarie.accounts.util.AppConfig
import io.gatling.core.Predef._
import io.gatling.core.structure.ScenarioBuilder

object TAMServicesAccounts {
    private val url: String = "/uat1/cardsservices/rest/v2/accounts"
    private val accountFeed = csv(AppConfig.getTestVolumeConfig(Scenarios.TAM_SERVICES_ACCOUNTS.toString.toLowerCase(), "dataFile")).circular.random
    private val requestHandler: RequestHandler = new RequestHandler(url)

    def execute(): ScenarioBuilder = {
        val parameters = Map("mac" -> "${mac}")
        val positionsScenario: ScenarioBuilder = scenario("TAMServices")
          .feed(accountFeed)
          .exec(requestHandler.getTAMServicesAccountsRequest("TAMServices", parameters))
        positionsScenario
    }
}
