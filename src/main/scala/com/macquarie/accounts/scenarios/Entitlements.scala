package com.macquarie.accounts.scenarios

import com.macquarie.accounts.config.Scenarios
import com.macquarie.accounts.handler.RequestHandler
import com.macquarie.accounts.util.AppConfig
import io.gatling.core.Predef._
import io.gatling.core.structure.ScenarioBuilder

object Entitlements {

    private val url: String = "/access-management/v1/entitlements"
    private val macFeed = csv(AppConfig.getTestVolumeConfig(Scenarios.ENTITLEMENTS.toString.toLowerCase(), "dataFile")).random.circular
    private val requestHandler: RequestHandler = new RequestHandler(url)

    def execute(): ScenarioBuilder = {
        val parameters = Map("principal_id" -> "${macId}", "iv_groups" -> "no_cards")
        val entitlementScenario: ScenarioBuilder = scenario("Entitlements")
          .feed(macFeed)
          .exec(requestHandler.getRequest("Entitlements", parameters))
        entitlementScenario
    }
}
