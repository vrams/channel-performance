package com.macquarie.accounts.simulations

import com.macquarie.accounts.config.Scenarios
import com.macquarie.accounts.scenarios._
import com.macquarie.accounts.util.AppConfig
import com.typesafe.config.ConfigFactory
import com.typesafe.scalalogging.LazyLogging
import io.gatling.app.Gatling
import io.gatling.core.Predef._
import io.gatling.core.structure.PopulationBuilder
import io.gatling.http.Predef.http
import io.gatling.http.protocol.HttpProtocolBuilder

import scala.concurrent.duration._

class PerformanceTestSimulation extends Simulation with LazyLogging {

    private val environment: String = AppConfig.getTestEnvironment
    private val testType: String = AppConfig.getRuntimeConfig("testType")
    private val runDuration: Double = AppConfig.getTestTypeConfig(testType, "runDuration").toDouble

    before {
        logger.info("Accounts API Performance Test ('{}') started in '{}'.", testType, environment)
        logger.info("Test Duration: {} mins.", runDuration)
    }

    def buildProtocol(scenarioName: String): HttpProtocolBuilder = {
        val httpBuilder: HttpProtocolBuilder = http
          .baseURL("http://" + AppConfig.getTestVolumeConfig(scenarioName, "api") + AppConfig.getRuntimeConfig("endpoints." + environment + ".openShift"))
          .acceptHeader("application/json")
          .acceptEncodingHeader("gzip, deflate")
        httpBuilder
    }

  def buildTAMProtocol(scenarioName: String): HttpProtocolBuilder = {
    val httpBuilder: HttpProtocolBuilder = http
      .baseURL("http://" + AppConfig.getTestVolumeConfig(scenarioName, "api"))
      .header("Content-Type","application/json")
      .authorizationHeader("Basic Y2FyZHNzZXJ2aWNlX3VhdF90b2tlbjp2ZjNyYWswM1d4UGg5NTc=")
      .acceptHeader("application/json")
      .acceptEncodingHeader("gzip, deflate")
    httpBuilder
  }

    def buildApigeeProtocol(scenarioName: String): HttpProtocolBuilder = {
        val httpBuilder: HttpProtocolBuilder = http
          .baseURL(AppConfig.getRuntimeConfig("endpoints." + environment + ".apigee"))
          .acceptHeader("application/json")
          .acceptEncodingHeader("gzip, deflate")
        httpBuilder
    }

    def buildSAPProtocol(scenarioName: String): HttpProtocolBuilder = {
        val httpBuilder: HttpProtocolBuilder = http
          .baseURL(AppConfig.getRuntimeConfig("endpoints." + environment + ".sap"))
          .connectionHeader("Keep-Alive")
          .acceptHeader("*/*")
          .acceptEncodingHeader("gzip, deflate")
        httpBuilder
    }

    def buildSSLSAPProtocol(scenarioName: String): HttpProtocolBuilder = {
      val httpBuilder: HttpProtocolBuilder = http
        .disableClientSharing
        .baseURL(AppConfig.getRuntimeConfig("endpoints." + environment + ".sap"))
        .connectionHeader("Keep-Alive")
        .acceptHeader("*/*")
        .acceptEncodingHeader("gzip, deflate")
      httpBuilder
    }


    private def entitlementsScenario: PopulationBuilder = {
        val name: String = Scenarios.ENTITLEMENTS.toString.toLowerCase()
        val users: Int = AppConfig.getTestVolumeConfig(name, "users").toInt
        val txnPerSec: Int = AppConfig.getTestVolumeConfig(name, "txnPerSec").toInt
        val throttleTime: FiniteDuration = AppConfig.getTestVolumeConfig(name, "throttleTime").toInt.second
        val scenario: PopulationBuilder = Entitlements.execute()
          .inject(constantUsersPerSec(users) during runDuration.minute)
          .throttle(reachRps(txnPerSec) in throttleTime, holdFor(runDuration.minute))
          .protocols(buildProtocol(name))
        scenario
    }

    private def positionsScenario: PopulationBuilder = {
        val name: String = Scenarios.POSITIONS.toString.toLowerCase()
        val users: Int = AppConfig.getTestVolumeConfig(name, "users").toInt
        val txnPerSec: Int = AppConfig.getTestVolumeConfig(name, "txnPerSec").toInt
        val throttleTime: FiniteDuration = AppConfig.getTestVolumeConfig(name, "throttleTime").toInt.second
        val scenario: PopulationBuilder = Positions.execute()
          .inject(constantUsersPerSec(users) during runDuration.minute)
          .throttle(reachRps(txnPerSec) in throttleTime, holdFor(runDuration.minute))
          .protocols(buildProtocol(name))
        scenario
    }

    private def sapPositionsScenario: PopulationBuilder = {
        val name: String = Scenarios.SAP_POSITIONS.toString.toLowerCase()
        val users: Int = AppConfig.getTestVolumeConfig(name, "users").toInt
        val txnPerSec: Int = AppConfig.getTestVolumeConfig(name, "txnPerSec").toInt
        val throttleTime: FiniteDuration = AppConfig.getTestVolumeConfig(name, "throttleTime").toInt.second
        val scenario: PopulationBuilder = SAPPositions.execute()
          .inject(constantUsersPerSec(users) during runDuration.minute)
          .throttle(reachRps(txnPerSec) in throttleTime, holdFor(runDuration.minute))
          .protocols(buildSAPProtocol(name))
        scenario
    }

    private def sapSSLPositionsScenario: PopulationBuilder = {
      val name: String = Scenarios.SAP_POSITIONS.toString.toLowerCase()
      val users: Int = AppConfig.getTestVolumeConfig(name, "users").toInt
      val txnPerSec: Int = AppConfig.getTestVolumeConfig(name, "txnPerSec").toInt
      val throttleTime: FiniteDuration = AppConfig.getTestVolumeConfig(name, "throttleTime").toInt.second
      val scenario: PopulationBuilder = SSLSAPPositions.execute()
        .inject(constantUsersPerSec(users) during runDuration.minute)
        .throttle(reachRps(txnPerSec) in throttleTime, holdFor(runDuration.minute))
        .protocols(buildSSLSAPProtocol(name))
      scenario
    }

    private def getCardServiceAccount: PopulationBuilder = {
        val name: String = Scenarios.CARD_SERVICES_ACCOUNTS.toString.toLowerCase()
        val users: Int = AppConfig.getTestVolumeConfig(name, "users").toInt
        val txnPerSec: Int = AppConfig.getTestVolumeConfig(name, "txnPerSec").toInt
        val throttleTime: FiniteDuration = AppConfig.getTestVolumeConfig(name, "throttleTime").toInt.second
        val scenario: PopulationBuilder = CardServicesAccounts.execute()
          .inject(constantUsersPerSec(users) during runDuration.minute)
          .throttle(reachRps(txnPerSec) in throttleTime, holdFor(runDuration.minute))
          .protocols(buildProtocol(name))
        scenario
    }


  private def getTAMServiceAccount: PopulationBuilder = {
    val name: String = Scenarios.TAM_SERVICES_ACCOUNTS.toString.toLowerCase()
    val users: Int = AppConfig.getTestVolumeConfig(name, "users").toInt
    val txnPerSec: Int = AppConfig.getTestVolumeConfig(name, "txnPerSec").toInt
    val throttleTime: FiniteDuration = AppConfig.getTestVolumeConfig(name, "throttleTime").toInt.second
    Gatling
    val scenario: PopulationBuilder = TAMServicesAccounts.execute()
      .inject(constantUsersPerSec(users) during runDuration.minute)
      .throttle(reachRps(txnPerSec) in throttleTime, holdFor(runDuration.minute))
      .protocols(buildTAMProtocol(name))
    scenario
  }

    setUp(positionsScenario).maxDuration(runDuration.minute)

    after {
        logger.info("Accounts API Performance Test ('{}') completed in '{}'.", testType, environment)
    }
}
