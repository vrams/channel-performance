package com.macquarie.accounts.handler

import io.gatling.core.Predef._
import io.gatling.core.structure.ChainBuilder
import io.gatling.http.Predef._

class RequestHandler(uri: String) {

    def getRequest(requestName: String, parameters: Map[String, String]): ChainBuilder = {
        val messageBuilder = exec(http(requestName)
            .get(s"$uri")
            .queryParamMap(parameters)
            .check(status.is(200))
            .check(jsonPath("$.errors[*]").count.is(0))
        )
        messageBuilder
    }

    def getRequestWithHeaders(requestName: String, headers: Map[String, String], parameters: Map[String, String]): ChainBuilder = {
        val messageBuilder = exec(http(requestName)
            .get(s"$uri")
            .headers(headers)
            .disableUrlEncoding
            .queryParamMap(parameters)
            .check(status.is(200))
            .check(jsonPath("$.errors[*]").count.is(0))
        )
        messageBuilder
    }

    def getSAPRequest(requestName: String, userName: String, password: String, parameters: Map[String, String]): ChainBuilder = {
        val messageBuilder = exec(http(requestName)
          .get(s"$uri")
            .basicAuth(userName, password)
          .queryParamMap(parameters)
          .check(status.is(200))
        )
        messageBuilder
    }

    def getSSLSAPRequest(requestName: String, parameters: Map[String, String]): ChainBuilder = {
        val messageBuilder = exec(http(requestName)
          .get(s"$uri")
          .queryParamMap(parameters)
          .check(status.is(200))
        )
        messageBuilder
    }

    def getCardServicesAccountsRequest(requestName: String, parameters: Map[String, String]): ChainBuilder = {
        val messageBuilder = exec(http(requestName)
          .get(s"$uri")
          .queryParamMap(parameters)
          .check(status.is(200))
        )
        messageBuilder
    }

    def getTAMServicesAccountsRequest(requestName: String, parameters: Map[String, String]): ChainBuilder = {
        val messageBuilder = exec(http(requestName)
          .get(s"$uri")
          .queryParamMap(parameters)
          .check(status.is(200))
        )
        messageBuilder
    }

    def postRequest(requestName: String, formParameters: Map[String, String]): ChainBuilder = {
        val requestBuilder = exec(http(requestName)
            .post(s"$uri")
            .formParamMap(formParameters).asFormUrlEncoded
            .check(status.is(200))
            .check(jsonPath("$.access_token").saveAs("accessToken"))
            .check(jsonPath("$.refresh_token").saveAs("refreshToken"))
        )
        requestBuilder
    }
}
