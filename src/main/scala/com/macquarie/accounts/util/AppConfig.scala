package com.macquarie.accounts.util

import java.io.File

import com.typesafe.config.ConfigFactory

object AppConfig {

    private val configFile: File = new File("perftest.conf")
    private val config = ConfigFactory.parseFile(configFile).getConfig("app")

    def getTestEnvironment: String = {
        config.getString("testEnvironment")
    }

    def getRuntimeConfig(key: String): String = {
        config.getString("runtimeConfig." + key)
    }

    def getTestTypeConfig(testType: String, key: String): String = {
        config.getString("testTypeConfig." + testType + "." + key)
    }

    def getTestVolumeConfig(scenarioName: String, key: String): String = {
        config.getString("testVolumeConfig." + scenarioName + "." + key)
    }
}
