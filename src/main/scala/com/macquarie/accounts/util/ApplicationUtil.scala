package com.macquarie.accounts.util

import java.io.File

import org.apache.commons.io.FileUtils

import scala.util.Random

object ApplicationUtil {

    def getRandomNumber(min: Int, max: Int): Int = {
        val random = new Random()
        val randNo: Int = random.nextInt((max - min) + 1) + min
        randNo
    }

    def createFolder(folderPath: String): Unit = {
        val newDirectory = new File(folderPath)
        if (!newDirectory.exists()) {
            FileUtils.forceMkdir(newDirectory)
        }
    }
}
