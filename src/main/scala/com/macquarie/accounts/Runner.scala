package com.macquarie.accounts

import com.macquarie.accounts.simulations.PerformanceTestSimulation
import com.macquarie.accounts.util.{AppConfig, ApplicationUtil}
import com.typesafe.config.ConfigFactory
import io.gatling.app.Gatling
import io.gatling.core.config.GatlingPropertiesBuilder

object Runner {

  private val testType: String = AppConfig.getRuntimeConfig("testType")
  private val environment: String = AppConfig.getTestEnvironment
  private val resultsFolder = AppConfig.getRuntimeConfig("resultsFolder")
  private val dataFolder = AppConfig.getRuntimeConfig("dataFolder")

  def main(args: Array[String]): Unit = {
    ConfigFactory.invalidateCaches()
    ConfigFactory.load("gatling.conf")
    ApplicationUtil.createFolder(resultsFolder)
    val simulationClass = classOf[PerformanceTestSimulation].getName
    val propertiesBuilder = new GatlingPropertiesBuilder
    propertiesBuilder.simulationClass(simulationClass)
    propertiesBuilder.sourcesDirectory("./src/main/scala")
    propertiesBuilder.binariesDirectory("./target/classes")
    propertiesBuilder.runDescription(AppConfig.getRuntimeConfig("runDescription") + " " + testType + " - " + environment)
    propertiesBuilder.dataDirectory(dataFolder)
    propertiesBuilder.resultsDirectory(resultsFolder)
    propertiesBuilder.outputDirectoryBaseName(AppConfig.getRuntimeConfig("outputDirectoryName") + testType + "_" + environment)
    System.setProperty("jsse.enableSNIExtension", "false")

    Gatling.fromMap(propertiesBuilder.build)
  }
}
