package com.macquarie.accounts.config

object Scenarios extends Enumeration {
    type Scenarios = Value
    val ENTITLEMENTS, POSITIONS, QUICK_VIEW, SAP_POSITIONS, CARD_SERVICES_ACCOUNTS, TAM_SERVICES_ACCOUNTS = Value
}
